<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Script to redirect to an Amazon S3 file with an authenticated request.
 *
 * @package    repository_s3links
 * @copyright  2016 CV&A Consulting
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once('../../config.php');
require_once($CFG->dirroot . '/repository/s3links/lib.php');

$repoid = required_param('repoid', PARAM_INT);
$path = required_param('path', PARAM_RAW);

$repo = repository::get_repository_by_id($repoid, CONTEXT_SYSTEM);
if ($repo->instance->repositorytype !== 's3links') {
    throw new repository_exception('invalidplugin', 'repository', '', $repo->instance->repositorytype);
}

$repo->force_login();

$url = $repo->get_authenticated_url($path);

// Prevent debug errors - make sure context is properly initialised.
$PAGE->set_context(null);
$PAGE->set_pagelayout('redirect');  // No header and footer needed.
$PAGE->set_title(get_string('pageshouldredirect', 'moodle'));

// Make sure the session is closed properly, this prevents problems in IIS
// and also some potential PHP shutdown issues.
\core\session\manager::write_close();

@header($_SERVER['SERVER_PROTOCOL'] . ' 303 See Other');
@header('Location: ' . $url);
echo bootstrap_renderer::plain_redirect_message($url);
exit;
