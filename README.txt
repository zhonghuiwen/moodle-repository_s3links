Amazon S3 links repository for Moodle
=====================================

This repository plugin for Moodle allows to link files in Amazon S3, instead of
copying them to Moodle. Requests are authenticated, so it works for private
files. Only logged in users can access linked files.

You can configure multiple instances of the repository, each one tied to an
specific endpoint and bucket.

Developed by CV&A Consulting: http://www.cvaconsulting.com
