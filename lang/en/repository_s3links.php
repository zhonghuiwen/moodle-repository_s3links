<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

$string['configplugin'] = 'Amazon S3 links settings';
$string['pluginname'] = 'Amazon S3 links';
$string['s3links:view'] = 'View Amazon S3 links repository';
$string['accesskey'] = 'Access key';
$string['secretkey'] = 'Secret key';
$string['endpoint'] = 'Endpoint';
$string['customendpoint'] = 'Custom Endpoint';
$string['customendpointhttps'] = 'Custom endpoint uses https';
$string['bucket'] = 'Bucket';
$string['forcelogin'] = 'Force login';
$string['forcelogin_help'] = 'Only logged in users will be able to access links to this repository. Course-level
repositories will only be accessible by users enrolled in the course.';
$string['needaccesskey'] = 'Access key must be provided.';
$string['bucketnotfound'] = 'This bucket does not exist.';
$string['accesserror'] = 'Could not connect to Amazon S3 with these credentials.';
