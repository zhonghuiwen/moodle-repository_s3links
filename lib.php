<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

require_once($CFG->dirroot . '/repository/lib.php');
require_once($CFG->dirroot . '/repository/s3/S3.php');

/**
 * Repository class used to browse an Amazon S3 bucket.
 *
 * @package    repository_s3links
 * @copyright  2016 CV&A Consulting
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class repository_s3links extends repository {

    /**
     * Return names of the instance options.
     *
     * @return array
     */
    public static function get_instance_option_names() {
        return array('pluginname', 'accesskey', 'secretkey', 'endpoint', 'customendpoint', 'customendpointhttps', 'bucket', 'forcelogin');
    }

    /**
     * Validate repository plugin instance form
     *
     * @param moodleform $mform moodle form
     * @param array $data form data
     * @param array $errors errors
     * @return array errors
     */
    public static function instance_config_form($mform, $classname = 'repository') {
        $mform->addElement('text', 'accesskey', get_string('accesskey', 'repository_s3links'));
        $mform->setType('accesskey', PARAM_RAW_TRIMMED);
        $mform->addRule('accesskey', get_string('required'), 'required', null, 'client');

        $mform->addElement('text', 'secretkey', get_string('secretkey', 'repository_s3links'));
        $mform->setType('secretkey', PARAM_RAW_TRIMMED);
        $mform->addRule('secretkey', get_string('required'), 'required', null, 'client');

        // List of S3 endpoints: http://docs.aws.amazon.com/general/latest/gr/rande.html#s3_region
        $endpointselect = array(
            "s3.amazonaws.com" => "s3.amazonaws.com",
            "s3-external-1.amazonaws.com" => "s3-external-1.amazonaws.com",
            "s3-us-west-2.amazonaws.com" => "s3-us-west-2.amazonaws.com",
            "s3-us-west-1.amazonaws.com" => "s3-us-west-1.amazonaws.com",
            "s3-eu-west-1.amazonaws.com" => "s3-eu-west-1.amazonaws.com",
            "s3.eu-central-1.amazonaws.com" => "s3.eu-central-1.amazonaws.com",
            "s3-eu-central-1.amazonaws.com" => "s3-eu-central-1.amazonaws.com",
            "s3-ap-southeast-1.amazonaws.com" => "s3-ap-southeast-1.amazonaws.com",
            "s3-ap-southeast-2.amazonaws.com" => "s3-ap-southeast-2.amazonaws.com",
            "s3-ap-northeast-1.amazonaws.com" => "s3-ap-northeast-1.amazonaws.com",
            "s3-sa-east-1.amazonaws.com" => "s3-sa-east-1.amazonaws.com",
            "custom-endpoint" => "Custom Endpoint"
        );
        $mform->addElement('select', 'endpoint', get_string('endpoint', 'repository_s3links'), $endpointselect);
        $mform->setDefault('endpoint', 's3.amazonaws.com'); // Default to US Endpoint.
        $mform->addRule('endpoint', get_string('required'), 'required', null, 'client');

        $mform->addElement('text', 'customendpoint', get_string('customendpoint', 'repository_s3links'));
        $mform->setType('customendpoint', PARAM_RAW_TRIMMED);

        $mform->addElement('checkbox', 'customendpointhttps', get_string('customendpointhttps', 'repository_s3links'));
        $mform->setType('customendpointhttps', PARAM_BOOL);
        $mform->setDefault('customendpointhttps', true);

        $mform->addElement('text', 'bucket', get_string('bucket', 'repository_s3links'));
        $mform->setType('bucket', PARAM_RAW_TRIMMED);
        $mform->addRule('bucket', get_string('required'), 'required', null, 'client');

        $mform->addElement('checkbox', 'forcelogin', get_string('forcelogin', 'repository_s3links'));
        $mform->setType('forcelogin', PARAM_BOOL);
        $mform->setDefault('forcelogin', true);
        $mform->addHelpButton('forcelogin', 'forcelogin', 'repository_s3links');
    }

    /**
     * Validate repository plugin instance form
     *
     * @param moodleform $mform moodle form
     * @param array $data form data
     * @param array $errors errors
     * @return array errors
     */
    public static function instance_form_validation($mform, $data, $errors) {
        $endpoint = $data['endpoint'];
        if ($data['endpoint'] === 'custom-endpoint') $endpoint = $data['customendpoint'];
        $s3 = self::init_s3($data['accesskey'], $data['secretkey'], $endpoint);
        try {
            $buckets = $s3->listBuckets();
            if (!in_array($data['bucket'], $buckets)) {
                $errors['bucket'] = get_string('bucketnotfound', 'repository_s3links');
            }
        } catch (S3Exception $e) {
            $errors['accesskey'] = get_string('accesserror', 'repository_s3links');
            throw $e;
        }
        return $errors;
    }

    /**
     * Return file URL, for most plugins, the parameter is the original
     * url, but some plugins use a file id, so we need this function to
     * convert file id to original url.
     *
     * @param string $url the url of file
     * @return string
     */
    public function get_link($source) {
        $params = array('repoid' => $this->id, 'path' => $source);
        $url = new moodle_url('/repository/s3links/redirect.php', $params);
        return $url->out(false);
    }

    /**
     * This S3 plugin always returns links
     *
     * @return int
     */
    public function supported_returntypes() {
        return FILE_EXTERNAL;
    }

    /**
     * S3 repositories do not contain private user data.
     * @return boolean
     */
    public function contains_private_data() {
        return false;
    }

    /**
     * Get S3 file list
     *
     * @param string $path
     * @return array The file list and options
     */
    public function get_listing($path = '', $page = '') {
        global $CFG, $OUTPUT;

        $accesskey = $this->get_option('accesskey');
        $secretkey = $this->get_option('secretkey');
        $endpoint = $this->get_option('endpoint');
        if($endpoint === 'custom-endpoint') $endpoint = $this->get_option('customendpoint');
        $s3 = self::init_s3($accesskey, $secretkey, $endpoint);

        $list = array();
        $list['list'] = array();
        $list['path'] = array(
            array('name' => get_string('pluginname', 'repository_s3links'), 'path' => '')
        );
        $list['manage'] = false;
        $list['dynload'] = true;
        $list['nologin'] = true;
        $list['nosearch'] = true;

        $tree = array();
        $files = array();
        $folders = array();

        try {
            $contents = $s3->getBucket($this->get_option('bucket'), $path, null, null, '/', true);
        } catch (S3Exception $e) {
            throw new moodle_exception(
                'errorwhilecommunicatingwith',
                'repository',
                '',
                $this->get_name(),
                $e->getMessage()
            );
        }

        foreach ($contents as $object) {

            // If object has a prefix, it is a 'CommonPrefix', which we consider a folder
            if (isset($object['prefix'])) {
                $title = rtrim($object['prefix'], '/');
            } else {
                $title = $object['name'];
            }

            // Removes the prefix (folder path) from the title
            if (strlen($path) > 0) {
                $title = substr($title, strlen($path));
                // Check if title is empty and not zero
                if (empty($title) && !is_numeric($title)) {
                    // Amazon returns the prefix itself, we skip it
                    continue;
                }
            }

            // This is a so-called CommonPrefix, we consider it as a folder
            if (isset($object['prefix'])) {
                $folders[] = array(
                    'title' => $title,
                    'children' => array(),
                    'thumbnail' => $OUTPUT->pix_url(file_folder_icon(90))->out(false),
                    'path' => $object['prefix']
                );
            } else {
                $files[] = array(
                    'title' => $title,
                    'size' => $object['size'],
                    'datemodified' => $object['time'],
                    'source' => $object['name'],
                    'thumbnail' => $OUTPUT->pix_url(file_extension_icon($title, 90))->out(false)
                );
            }
        }

        $tree = array_merge($folders, $files);

        $trail = '';
        if (!empty($path)) {
            $parts = explode('/', $path);
            if (count($parts) > 1) {
                foreach ($parts as $part) {
                    if (!empty($part)) {
                        $trail .= $part . '/';
                        $list['path'][] = array('name' => $part, 'path' => $trail);
                    }
                }
            } else {
                $list['path'][] = array('name' => $path, 'path' => $path);
            }
        }

        $list['list'] = $tree;

        return $list;
    }

    /**
     * Force user to log in if needed.
     */
    public function force_login() {
        if ($this->get_option('forcelogin')) {
            $context = context::instance_by_id($this->instance->contextid);
            if ($context->contextlevel == CONTEXT_COURSE) {
                require_login($context->instanceid);
            } else {
                require_login();
            }
        }
    }

    /**
     * Builds an authenticated URL for a file in S3.
     * @return string
     */
    public function get_authenticated_url($path) {
        global $CFG;

        $now = time();
        $expires = !empty($CFG->sessiontimeout) ? $CFG->sessiontimeout : 7200;
        $accesskey = $this->get_option('accesskey');
        $secretkey = $this->get_option('secretkey');
        $endpoint = $this->get_option('endpoint');
        $isCustomEndpoint = false;
        if($endpoint === 'custom-endpoint') {
            $isCustomEndpoint = true; 
            $endpoint = $this->get_option('customendpoint');
        }
        $bucket = $this->get_option('bucket');
        $https = $isCustomEndpoint ? $this->get_option('customendpointhttps') : true;

        // Get region
        $region = 'us-east-1';
        if (preg_match('/^s3-(.*)\.amazonaws\.com$/', $endpoint, $match)) {
            if ($match[1] !== 'external-1') {
                $region = $match[1];
            }
        }

        // Query parameters
        $params = array(
            'X-Amz-Algorithm' => 'AWS4-HMAC-SHA256',
            'X-Amz-Credential' => $accesskey . '/' . gmdate('Ymd', $now) . '/' . $region . '/s3/aws4_request',
            'X-Amz-Date' => gmdate('Ymd\\THis\\Z', $now),
            'X-Amz-Expires' => $expires,
            'X-Amz-SignedHeaders' => 'host',
        );

        // Encode path
        $parts = explode('/', $path);
        foreach ($parts as $i => $part) {
            $parts[$i] = rawurlencode($part);
        }
        $path = implode('/', $parts);

        // Create signature
        $request = "GET\n"
            . "/{$bucket}/{$path}\n"
            . http_build_query($params, '', '&') . "\n"
            . "host:{$endpoint}\n"
            . "\n"
            . "host\n"
            . "UNSIGNED-PAYLOAD";
        $data = "AWS4-HMAC-SHA256\n"
            . gmdate('Ymd\\THis\\Z', $now) . "\n"
            . gmdate('Ymd', $now) . '/' . $region . '/s3/aws4_request' . "\n"
            . hash('sha256', $request);
        $key = hash_hmac('sha256', gmdate('Ymd', $now), 'AWS4' . $secretkey, true);
        $key = hash_hmac('sha256', $region, $key, true);
        $key = hash_hmac('sha256', 's3', $key, true);
        $key = hash_hmac('sha256', 'aws4_request', $key, true);
        $params['X-Amz-Signature'] = hash_hmac('sha256', $data, $key);

        $querystring = http_build_query($params, '', '&');
        return ($https ? 'https://' : 'http://' ) . $endpoint . '/' . $bucket .'/' . $path . '?' . $querystring;
    }

    protected static function init_s3($accesskey, $secretkey, $endpoint) {
        global $CFG;

        $s3 = new S3($accesskey, $secretkey, false, $endpoint);
        $s3->setExceptions(true);

        // Port of curl::__construct().
        if (!empty($CFG->proxyhost)) {
            if (empty($CFG->proxyport)) {
                $proxyhost = $CFG->proxyhost;
            } else {
                $proxyhost = $CFG->proxyhost . ':' . $CFG->proxyport;
            }
            $proxytype = CURLPROXY_HTTP;
            $proxyuser = null;
            $proxypass = null;
            if (!empty($CFG->proxyuser) and !empty($CFG->proxypassword)) {
                $proxyuser = $CFG->proxyuser;
                $proxypass = $CFG->proxypassword;
            }
            if (!empty($CFG->proxytype) && $CFG->proxytype == 'SOCKS5') {
                $proxytype = CURLPROXY_SOCKS5;
            }
            $s3->setProxy($proxyhost, $proxyuser, $proxypass, $proxytype);
        }

        return $s3;
    }
}
